CREATE TABLE `medical_studies` (
  `idmedical_studies` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(1000) NOT NULL,
  `duration` INT NOT NULL,
  `price` DOUBLE NOT NULL,
  `id_laboratories` INT NOT NULL,
  PRIMARY KEY (`idmedical_studies`))
ENGINE = InnoDB;