package com.softtek.msacademy.Service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.Controller.MedicalStudyNotFound;
import com.softtek.msacademy.entity.MedicalStudy;
import com.softtek.msacademy.repository.MedicalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty(name = "coreSize", value = "10"),
                @HystrixProperty(name = "maxQueueSize", value = "-1")
        },
        commandProperties = {
                @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "20"),
                @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
                @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000"),
                @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "10")
        }
)
public class MedicalStudyServiceImpl implements MedicalStudyService {

    @Autowired
    MedicalRepository medicalRepository;

    @HystrixCommand(threadPoolKey = "addThreadPool")
    @Override
    public MedicalStudy addMedicalStudy(MedicalStudy medicalStudy) {
        return medicalRepository.save(medicalStudy);
    }

    @HystrixCommand(threadPoolKey = "updateThreadPool")
    @Override
    public MedicalStudy updateMedicalStudy(MedicalStudy medicalStudy) {
        Optional<MedicalStudy> medicalDetails = Optional.ofNullable(this.medicalRepository.findById(medicalStudy.getId()));

        MedicalStudy medicalStudyUpdate=medicalDetails.get();
        medicalStudyUpdate.setId(medicalStudy.getId());
        medicalStudyUpdate.setPrice(medicalStudy.getPrice());
        medicalStudyUpdate.setName(medicalStudy.getName());
        medicalStudyUpdate.setDuration(medicalStudy.getDuration());
        medicalStudyUpdate.setDescription(medicalStudy.getDescription());
        medicalStudyUpdate.setId_laboratories(medicalStudy.getId_laboratories());
        medicalRepository.save(medicalStudy);

        //final MedicalStudy updatedMedicalStudy = medicalRepository.save(medicalStudy);
        return medicalStudyUpdate;
    }


    @HystrixCommand(threadPoolKey = "getThreadPool")
    @Override
    public MedicalStudy getMedicalStudy(int id) {
        Optional<MedicalStudy> medicalStudyOb = Optional.ofNullable(this.medicalRepository.findById(id));
        if (!medicalStudyOb.isPresent())
            throw new MedicalStudyNotFound("This ID doesn´t exist: " + id);
        return medicalStudyOb.get();
    }

    public void getAllStudyRecords(){
        Optional<List<MedicalStudy>> medicalStudyOb = Optional.ofNullable(this.medicalRepository.findAll());

    }

}
