package com.softtek.msacademy.Service;

import com.softtek.msacademy.entity.MedicalStudy;

public interface MedicalStudyService {
    MedicalStudy addMedicalStudy (MedicalStudy medicalStudy);
    MedicalStudy updateMedicalStudy (MedicalStudy medicalStudy);
    MedicalStudy getMedicalStudy (int id);
    void getAllStudyRecords();
}
