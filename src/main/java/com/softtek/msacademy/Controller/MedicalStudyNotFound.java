package com.softtek.msacademy.Controller;

public class MedicalStudyNotFound extends RuntimeException  {
    public MedicalStudyNotFound(String exception) {
        super(exception);
    }
}
