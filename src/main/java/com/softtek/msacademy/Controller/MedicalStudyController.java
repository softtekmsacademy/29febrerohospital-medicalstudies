package com.softtek.msacademy.Controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.entity.MedicalStudy;
import com.softtek.msacademy.repository.MedicalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.softtek.msacademy.Controller.MedicalStudyNotFound;
import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.Optional;


@RestController


public class MedicalStudyController {

    @Autowired
    private MedicalRepository medicalRepository;


    @PostMapping("/medicalStudies")
    public MedicalStudy addMedicalStudy(@Valid @RequestBody MedicalStudy medicalStudy){
        return medicalRepository.save(medicalStudy);
    }


    @PutMapping("/medicalStudies/{medicalStudyId}")
    public ResponseEntity<MedicalStudy> updateMedicalStudy(@PathVariable(value = "medicalStudyId")Integer medicalId,
                                                           @Valid @RequestBody MedicalStudy medicalDetails) {
        MedicalStudy medicalStudy = medicalRepository.findById(medicalId);

        medicalStudy.setDescription(medicalDetails.getDescription());
        medicalStudy.setDuration(medicalDetails.getDuration());
        medicalStudy.setName(medicalDetails.getName());
        medicalStudy.setPrice(medicalDetails.getPrice());
        medicalStudy.setId_laboratories(medicalDetails.getId_laboratories());
        final MedicalStudy updatedMedicalStudy = medicalRepository.save(medicalStudy);
        return ResponseEntity.ok(updatedMedicalStudy);
    }


    @GetMapping("/medicalStudies/{medicalStudyId}")
    public ResponseEntity<Optional<MedicalStudy>> getMedicalStudyId(@PathVariable(value = "medicalStudyId") Integer medicalId)
    {
        Optional<MedicalStudy> medicalStudy = Optional.ofNullable(medicalRepository.findById(medicalId));
        if (!medicalStudy.isPresent())
            throw new MedicalStudyNotFound("This ID doesn´t exist: " + medicalId);
        return ResponseEntity.ok().body(medicalStudy);
    }

    @GetMapping(value="/medicalStudies", produces = "application/json")
    public ResponseEntity getAllMedicalStudies() {
        if(medicalRepository.findAll().isEmpty()){
            throw new MedicalStudyNotFound("No study records found");
        }
        else {
            return new ResponseEntity(medicalRepository.findAll(), HttpStatus.OK);
        }
    }

}