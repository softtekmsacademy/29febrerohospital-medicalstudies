package com.softtek.msacademy.repository;


import com.softtek.msacademy.entity.MedicalStudy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MedicalRepository extends JpaRepository<MedicalStudy, Long> {
    public MedicalStudy findById(Integer id);
    public List<MedicalStudy> findAll();

}

